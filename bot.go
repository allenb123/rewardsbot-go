package main

import (
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"github.com/tebeka/selenium/firefox"
	"time"
	"math/rand"
	"errors"
)

type Bot struct {
	Driver selenium.WebDriver
}

func NewBot (url, browserName string, mobile bool) (*Bot, error) {
	capab := selenium.Capabilities{
		"browserName": browserName,
		firefox.CapabilitiesKey: map[string]interface{} {
			"args": []string{"--headless"},
		},
		chrome.CapabilitiesKey: map[string]interface{} {
			"args": []string{"--headless"},
		},
	}
	
	if mobile {
		capab[chrome.CapabilitiesKey].(map[string]interface{})["mobileEmulation"] = map[string]string{
			"deviceName": "Nexus 5",
		}
	}
	driver, err := selenium.NewRemote(capab, url)
	if err != nil {
		return nil, err
	}

	bot := new(Bot)
	bot.Driver = driver
	return bot, nil
}

func (bot Bot) Destroy() {
	bot.Driver.Quit()
}

func (bot Bot) SignIn(username, password string) error {
	if err := bot.Driver.Get("https://login.live.com/login.srf"); err != nil {
		return err
	}

	// Fill in the email input
	elem, err := bot.Driver.FindElement(selenium.ByXPATH, "//input[@name='loginfmt']");
	if err != nil {
		return err
	}
	if err = elem.SendKeys(username); err != nil {
		return err
	}

	// Click the "Next" button
	elem, err = bot.Driver.FindElement(selenium.ByID, "idSIButton9")	
	if err != nil {
		return err
	}
	if err = elem.Click(); err != nil {
		return err
	}

	time.Sleep(500 * time.Millisecond)

	// Fill in password
	elem, err = bot.Driver.FindElement(selenium.ByXPATH, "//input[@name='passwd']")
	if err != nil {
		return err
	}
	if err = elem.SendKeys(password); err != nil {
		return err
	}

	// Click "Sign in"
	elem, err = bot.Driver.FindElement(selenium.ByID, "idSIButton9")	
	if err != nil {
		return err
	}
	if err = elem.Click(); err != nil {
		return err
	}

	time.Sleep(500 * time.Millisecond)

	// Find error text
	elem, _ = bot.Driver.FindElement(selenium.ByXPATH, "//*[contains(@class, 'alert-error')]")
	if elem != nil {
		return errors.New("Wrong username or password")
	}
	
	return nil	
}

func (bot Bot) Search(words []string) error {
	if err := bot.Driver.Get("https://bing.com/"); err != nil {
		return err
	}

	for _, word := range words {
		time.Sleep(time.Duration(rand.Intn(2) + 2) * time.Second)
		bot.Driver.DismissAlert() // Sometimes random alerts pop up
	
		// Type in query
		elem, err := bot.Driver.FindElement(selenium.ByID, "sb_form_q")
		if err != nil {
			return err
		}
		if err := elem.Clear(); err != nil {
			return err
		}
		if err := elem.SendKeys(word); err != nil {
			return err
		}

		// Click search button
		elems, err := bot.Driver.FindElements(selenium.ByID, "sb_form_go")
		if err != nil {
			return err
		}

		if len(elems) == 0 {
			elem, err = bot.Driver.FindElement(selenium.ByID, "sbBtn")
		} else {
			elem = elems[0]
		}

		if err = elem.Click(); err != nil {
			return err
		}
	}

	return nil
}

func (bot Bot) DailySet() error {
	// Get reards page
	if err := bot.Driver.Get("https://account.microsoft.com/rewards/"); err != nil {
		return err
	}

	time.Sleep(1 * time.Second)
	// Click "Sign in with Microsoft" button
	elems, err := bot.Driver.FindElements(selenium.ByID, "signinhero")
	if err != nil {
		return err
	}
	if len(elems) > 0 {
		if err := elems[0].Click(); err != nil {
			return err
		}
	}
	
	// Wait until links are ready
	if err := bot.Driver.Wait(func(wd selenium.WebDriver) (bool, error) {
		elems, err := wd.FindElements(selenium.ByXPATH, "//mee-rewards-daily-set-item-content//a")
		return len(elems) > 1, err
	}); err != nil {
		return err
	}

	rewardsHandle, err := bot.Driver.CurrentWindowHandle()
	if err != nil {
		return err
	}

	// Find card links
	elems, err = bot.Driver.FindElements(selenium.ByXPATH, "//mee-rewards-daily-set-item-content//a")
	if err != nil {
		return err
	}

	// Click cards
	for i := 0; i < 3; i++ {
		if err := elems[i].Click(); err != nil {
			return err
		}

		time.Sleep(time.Duration(rand.Intn(2) + 2) * time.Second)

		if i < 2 {
			if err := bot.Driver.SwitchWindow(rewardsHandle); err != nil {
				return err
			}
		}

		if i == 2 {	
			handles, err := bot.Driver.WindowHandles()
			if err != nil {
				return err
			}
			
			// Switch to the window that isn't the rewards page
			for _, handle :=  range handles {
				if err := bot.Driver.SwitchWindow(handle); err != nil {
					return err
				}

				var elems []selenium.WebElement
				if err := bot.Driver.WaitWithTimeout(func(wd selenium.WebDriver) (bool, error) {
					elems, err = wd.FindElements(selenium.ByXPATH, "//a[contains(@class, 'pl_optionRow')]")
					return len(elems) > 0, err
				}, 3 * time.Second); err != nil {
					continue
				}
			
				if err := elems[rand.Intn(len(elems))].Click(); err != nil {
					return err
				}

				break				
			}				
		}
	}

	time.Sleep(time.Duration(rand.Intn(2) + 2) * time.Second)

	return nil
}
