package main
import (
	"fmt"
	"os"
	"math/rand"
	"time"
	"flag"
)

var url = flag.String("url", "http://127.0.0.1:9515", "Address of WebDriver server")
var actions = flag.Uint("a", 7, "Actions to take.\n  1 = PC search\n  2 = daily set\n  4 = mobile search (chromedriver only)\nBitwise or above actions to do multiple")
var username = flag.String("user", "", "Microsoft email or username")
var password = flag.String("pwd", "", "Microsoft password")
var browserName = flag.String("browser", "chrome", "Name of browser (e.g. firefox, chrome)")

var words = [1024]string{"genuine", "knock", "teach", "suitcase", "slave", "rob", "pollution", "foreigner", "impound", "value", "window", "coach", "monarch", "introduction", "screw", "bean", "confuse", "fall", "present", "abnormal", "miscarriage", "translate", "deprive", "complex", "study", "good", "bus", "undress", "sale", "rifle", "security", "available", "mistreat", "guerrilla", "sketch", "spring", "eye", "hall", "reduction", "publicity", "notion", "network", "warning", "divide", "railroad", "abridge", "large", "float", "runner", "shelter", "sister", "single", "bell", "rank", "depart", "elephant", "node", "domestic", "reader", "craft", "hypnothize", "spontaneous", "pain", "selection", "maze", "family", "contradiction", "excess", "compensation", "redeem", "initial", "offensive", "maximum", "wife", "visit", "minimum", "ride", "guard", "chalk", "wheat", "fund", "human", "body", "brake", "tree", "personality", "cow", "tender", "assumption", "eagle", "worry", "horn", "reduce", "manage", "battery", "fist", "beneficiary", "crevice", "exceed", "knife", "snuggle", "defendant", "election", "drop", "skate", "respectable", "choice", "lift", "frozen", "metal", "teacher", "executrix", "marsh", "need", "rape", "justify", "priority", "descent", "syndrome", "interest", "public", "deposit", "health", "context", "plug", "video", "powder", "mix", "stun", "bare", "chauvinist", "railcar", "marketing", "depend", "seminar", "century", "explain", "affair", "insurance", "like", "formulate", "lunch", "reckless", "discourage", "bite", "distance", "small", "governor", "fixture", "drain", "south", "miss", "model", "origin", "ritual", "march", "thaw", "filter", "cabin", "planet", "spill", "scenario", "twilight", "cunning", "technology", "mercy", "cancel", "looting", "miracle", "revolution", "misery", "investigation", "cathedral", "shiver", "blow", "shy", "flat", "supplementary", "legislature", "voter", "simplicity", "heat", "mold", "pigeon", "dip", "survivor", "function", "describe", "produce", "electronics", "bullet", "weight", "control", "weave", "constitution", "concentration", "tube", "guitar", "series", "thigh", "dividend", "drawing", "minor", "ballot", "belief", "keep", "layout", "snail", "rhetoric", "bleed", "bounce", "source", "fair", "economics", "notice", "pluck", "practical", "chew", "alarm", "ditch", "mechanism", "calorie", "ash", "confusion", "wrestle", "credibility", "treatment", "city", "veil", "sentence", "creed", "Bible", "hypothesis", "identification", "vote", "prosecution", "monster", "decrease", "banana", "trip", "stage", "talented", "advice", "document", "franchise", "hypothesize", "stake", "wood", "hostile", "environment", "meal", "contract", "narrow", "method", "even", "responsibility", "star", "amber", "strange", "access", "driver", 
"casualty", "requirement", "consciousness", "voucher", "ignorance", "humanity", "poem", "patience", "elite", "adopt", "flesh", "coerce", "cinema", "ancestor", "half", "predict", "common", "cage", "step", "shed", "performance", "leadership", "rotten", "wine", "agony", "pony", "theme", "trustee", "protest", "sport", "seem", "relinquish", "domination", "still", "soul", "guest", "load", "purpose", "element", "memorial", "cottage", "distant", "pin", "story", "basin", "exact", "twitch", "sense", "index", "qualified", "brag", "kill", "cooperate", "mountain", "facade", "cheek", "invisible", "prayer", "cook", "belt", "clock", "laundry", "gradual", "mechanical", "grand", "sulphur", "emotion", "nerve", "ostracize", "beer", "tower", "polite", "absorb", "silence", "ring", "gregarious", "spare", "Sunday", "cross", "bronze", "colon", "glasses", "ask", "resign", "assignment", "decline", "flexible", "enemy", "astonishing", "stretch", "mind", "fit", "animal", "fabricate", "agile", "automatic", "jail", "ratio", "harmful", "deer", "bench", "island", "help", "compliance", "arena", "sensitive", "drag", "prefer", "gain", "scheme", "class", "fluctuation", "wound", "breakfast", "suite", "glory", "cancer", "correction", "summary", "future", "tie", "hit", "wash", "negligence", "weapon", "flow", "disclose", "waist", "trainer", "effort", "global", "topple", "coin", "tape", "finance", "constitutional", "neutral", "north", "title", "range", "recommendation", "admire", "care", "progress", "face", "training", "grave", "chimpanzee", "comfortable", "debate", "colony", "strap", "multiply", "announcement", "estimate", "mention", "bucket", "company", "bake", "nun", "noble", "orthodox", "housewife", "acid", "packet", "discuss", "leaf", "shift", "officer", "fork", "tasty", "miner", "discover", "dynamic", "code", "connection", "peel", "admission", "aquarium", "sign", "courage", "quarrel", "jockey", "opponent", "permanent", "kitchen", "circumstance", "message", "self", "size", "glass", "tip", "fare", "dream", "rehearsal", "faithful", "climate", "fling", "stable", "temple", "fail", "double", "hole", "motivation", "center", "transmission", "brink", "secure", "exclusive", "explicit", "familiar", "pat", "episode", "hip", "remedy", "import", "dialect", "spin", "glide", "part", "chair", "entertain", "reflect", "favour", "wake", "output", "safari", "swop", "hero", "skin", "tray", "final", "smooth", "mosquito", "father", "omission", "apparatus", "permission", "senior", "width", "locate", "reliance", "palm", "kick", "break", "down", "spectrum", "lifestyle", "mystery", "motorist", "soak", "researcher", "eject", "move", "addition", "slant", "royalty", "deficit", "distinct", "thanks", "rotation", "drug", "heroin", "time", "key", "rush", "dedicate", "abuse", "acute", "hotdog", "ball", "example", "rocket", "dorm", "shelf", "wealth", "chocolate", "invite", "fastidious", "possible", "lamp", "plagiarize", "lace", "digress", "cope", "detector", "girlfriend", "identity", "gear", "majority", "restrain", "duke", "copyright", "block", "achievement", "tourist", "pan", "improve", "seasonal", "acquisition", "pot", "sheep", "riot", "volunteer", "border", "offend", "vegetable", "demonstrate", "production", "assembly", "party", "rain", "preparation", "yearn", "incident", "novel", "shrink", "relate", "nonsense", "collection", "gate", "lecture", "uncertainty", "fill", "relieve", "deviation", 
"transfer", "tin", "false", "mail", "carrier", "awful", "food", "rule", "professional", "ice", "texture", "habit", "rice", "chip", "tool", "multimedia", "hammer", "education", "superintendent", "drown", "paralyzed", "manual", "devote", "revolutionary", "occupation", "rent", "impress", "headline", "press", "slap", "love", "text", "swarm", "brick", "closed", "lump", "program", "established", "tendency", "machinery", "proclaim", "Europe", "realism", "hospital", "epicalyx", "result", "commerce", "reconcile", "reverse", "movement", "folklore", "instruction", "breathe", "morning", "current", "lighter", "cassette", "exercise", "stop", "vague", "tract", "lane", "patrol", "classify", "owner", "thinker", "thin", "trivial", "ruin", "cake", "slow", "direct", "mark", "classroom", "cherry", "find", "analysis", "sell", "solve", "wear", "contraction", "fresh", "aisle", "day", "ecstasy", "singer", "piano", "aloof", "impact", "loan", "egg", "white", "wisecrack", "census", "bathtub", "publication", "bag", "mill", "vain", "frequency", "extract", "distribute", "infection", "inappropriate", "conference", "normal", "charge", "primary", "fight", "perforate", "stroll", "quality", "strength", "ticket", "visible", "slump", "forest", "earthwax", "unity", "spider", "afford", "hook", "vacuum", "authorise", "extension", "worth", "steward", "rugby", "proportion", "formal", "reluctance", "confrontation", "factory", "lawyer", "realize", "account", "dribble", "aunt", "light", "heir", "pressure", "log", "style", "loot", "substitute", "system", "get", "retailer", "water", "spite", "exile", "physics", "brainstorm", "commemorate", "scream", "listen", "tax", "branch", "unlawful", "counter", "stir", "dealer", "menu", "home", "canvas", "fog", "mushroom", "duty", "profound", "tenant", "studio", "deliver", "dine", "excavate", "service", "diplomatic", "killer", "rate", "feedback", "release", "litigation", "urge", "resist", "coup", "inject", "sting", "few", "color-blind", "dull", "reason", "pawn", "retiree", "computing", "acquaintance", "pen", "aid", "roof", "kettle", "parade", "angle", "industry", 
"contain", "huge", "pension", "second", "suntan", "registration", "pump", "innovation", "horror", "dash", "clay", "impulse", "bloodshed", "volume", "appendix", "bubble", "coincide", "approve", "gallon", "channel", "evening", "national", "gas", "reproduce", "rider", "define", "discovery", "review", "update", "prove", "proud", "outside", "reign", "director", "secretion", "mood", "dare", "please", "remunerate", "direction", "hill", "houseplant", "copper", "tablet", "dominant", "rotate", "catalogue", "effect", "pest", "relax", "country", "buy", "handy", "basis", "archive", "veteran", "establish", "gutter", "green", "invasion", "audience", "print", "convince", "gasp", "greeting", "hunting", "trench", "slide", "shortage", "benefit", "giant", "grace", "convention", "vat", "main", "left", "amputate", "sensation", "taste", "poison", "building", "material", "expansion", "fox", "friend", "painter", "east", "opposed", "firm", "role", "predator", "murder", "pour", "cycle", "confession", "linear", "college", "freshman", "feign", "mutation", "nail", "rough", "feature", "allow", "disagreement", "lost", "eaux", "collar", "carry", "progressive", "celebration", "landscape", "basket", "warn", "confine", "basic", "active", "romantic", "treaty", "divorce", "cooperative", "shop", "unlike", "elegant", "pride", "smell", "sick", "easy", "garbage", "evolution", "show", "shock", "equinox", "distributor", "session", "bear", "parameter", "skilled", "crackpot", "restless", "policeman", "banner", "shout", "latest", "coma", "crowd", "buffet", "weigh", "high", "advertising", "junior", "panic", "sight", "fiction", "unaware", "talk", "dominate", "rare", "row", "perfect", "pie", "length", "opposite", "battle", "margin", "continuous", "great", "particle", "communication", "chart", "disco", "sequence", "pill", "compromise", "glare", "color", "adult", "speech", "complication", "throat", "sweep", "claim", "seed", "pupil", "AIDS", "theorist", "advance", "utter", "monk", "nationalism", "worm", "discrimination", "velvet", "blame", "clothes", "provision", "weak", "aware", "glimpse", "variety", "costume", "theft", "paragraph", "fee", "cap", "organisation", "banquet", "baby", "knee", "patch", "jewel", "exhibition", "case", "rage", "mine", "cheap", "firefighter", "certain", "convert", "learn", "tread", "unanimous", "psychology", "disappear", "concede", "pleasant", "beg", "joystick", "belly", "leaflet", "literacy", "determine", "lean", "prediction", "meadow", "underline"}

func init() {
	flag.Parse()

	if *username == "" || *password == "" {
		fmt.Fprintf(os.Stderr, "Error: -user or -pwd not set. Type `%s --help` for more information\n", os.Args[0])
		os.Exit(1)
	}

	rand.Seed(time.Now().UnixNano())
}

func DesktopActions() {
	bot, err := NewBot(*url, *browserName, false)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error connecting to webdriver: %s\n", err.Error())
		os.Exit(1)
	}
	defer bot.Destroy()

	if err := bot.SignIn(*username, *password); err != nil {
		fmt.Fprintf(os.Stderr, "Error signing in: %s\n", err.Error())
		os.Exit(1)
	}

	if *actions & 1 != 0 {
		rand.Shuffle(len(words), func(i, j int) {
			words[i], words[j] = words[j], words[i]		
		})

		if err := bot.Search(words[:30]); err != nil {
			fmt.Fprintf(os.Stderr, "Error searching: %s\n", err.Error())
		}
	}

	if *actions & 2 != 0 {
		if err := bot.DailySet(); err != nil {
			fmt.Fprintf(os.Stderr, "Error doing daily set: %s\n", err.Error())
		}
	}
}

func MobileActions() {
	bot, err := NewBot(*url, *browserName, true)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error connecting to server: %s\n", err.Error())
		os.Exit(1)
	}
	defer bot.Destroy()

	if err := bot.SignIn(*username, *password); err != nil {
		fmt.Fprintf(os.Stderr, "Error signing in: %s\n", err.Error())
		os.Exit(1)
	}

	rand.Shuffle(len(words), func(i, j int) {
		words[i], words[j] = words[j], words[i]		
	})

	if err := bot.Search(words[:20]); err != nil {
		fmt.Fprintf(os.Stderr, "Error searching: %s\n", err.Error())
	}
}

func main() {
	if *actions & (1 | 2) != 0 {
		DesktopActions()
	}

	if *actions & 4 != 0 {
		MobileActions()
	}
}
